{
    outputs = { self, nixpkgs }: {
        defaultPackage.x86_64-linux = 
            with import nixpkgs { system = "x86_64-linux"; };
            stdenv.mkDerivation {
                name = "banda-benchmark-bins";
                src = self + "/src";
                nativeBuildInputs = [
                    clang
                    gcc
                    zig
                    nim
                    ldc
                ];
                dontUnpack = true;
                buildPhase = "bash ${./bs/build.sh}";
                dontInstall = true;
            };
        
        packages.x86_64-linux.bench-shell = 
            with import nixpkgs { system = "x86_64-linux"; };
            mkShell {
                name = "banda-benchmark-plot";

                nativeBuildInputs = [
                    hyperfine
                    (python39.withPackages(ps: with ps; [ matplotlib ]))
                ];
            };
    };
}