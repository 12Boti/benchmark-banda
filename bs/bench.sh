set -e

[ -d ./result ] && bins=./result || bins=./build

[ "$1" ] && ROUNDS="-m $1" || ROUNDS=""
INPUT="input/2.txt"
EXECUTABLES=(
    clang.exe
    gcc.exe
    zig-alloc.exe
    zig-prealloc.exe
    nim.exe
    ldc.exe
)
COMMANDS=()
for e in "${EXECUTABLES[@]}"; do
    COMMANDS+=("$bins/$e < $INPUT")
done
COMMANDS+=(
    "pypy3 src/python/main.py < $INPUT"
    "python3 src/python/main.py < $INPUT"
)

hyperfine -w 5 $ROUNDS \
    --export-json results.json \
    "${COMMANDS[@]}"