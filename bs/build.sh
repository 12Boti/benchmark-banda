set -e

[ "$out" ] || out=./build
[ "$src" ] || src=./src

mkdir -p $out

clang++ -o $out/clang.exe -Ofast $src/cpp/main.cpp
g++ -o $out/gcc.exe -Ofast $src/cpp/main.cpp

mkdir -p build/zig-cache
ZIGFLAGS="-O ReleaseFast --cache-dir build/zig-cache --global-cache-dir build/zig-cache"
zig build-exe -femit-bin=$out/zig-alloc.exe $ZIGFLAGS $src/zig/alloc.zig
zig build-exe -femit-bin=$out/zig-prealloc.exe $ZIGFLAGS $src/zig/prealloc.zig

mkdir -p build/nim-cache
nim c -o:$out/nim.exe -d:danger --nimcache:build/nim-cache $src/nim/main.nim

ldc2 --of=$out/ldc.exe --release -O3 $src/d/main.d