import io
import json
import matplotlib.pyplot as plt

results = json.load(io.open("results.json"))["results"]
results.sort(key=lambda r: r["mean"])

#plt.ion()
fig, ax = plt.subplots()
ax.violinplot([r["times"] for r in results], showextrema=False, showmeans=True)
#ax.set_ylim(0, 0.05)
ax.set_xticks(range(1, len(results)+1))
ax.set_xticklabels([
    r["command"]
        .removeprefix("./result/")
        .removeprefix("./build/")
        .removesuffix(" < input/2.txt")
        .removesuffix(".exe")
    for r in results])
plt.show()