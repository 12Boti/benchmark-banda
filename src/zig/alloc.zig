const std = @import("std");
var allocator_impl = std.heap.GeneralPurposeAllocator(.{}){};
var allocator = &allocator_impl.allocator;
var raw_in: std.fs.File.Reader = undefined;
var in: std.io.BufferedReader(4096, @TypeOf(raw_in)).Reader = undefined;
var raw_out: std.fs.File.Writer = undefined;
var out: std.io.BufferedWriter(4096, @TypeOf(raw_out)).Writer = undefined;

fn readLine(buf: []u8) []u8 {
    return in.readUntilDelimiterOrEof(buf, '\n')
        catch unreachable orelse unreachable;
}

fn readUInt(comptime T: type) T {
    var line_buf: [255]u8 = undefined;
    var line = readLine(&line_buf);
    if (line[line.len-1] == '\r') {
        line = line[0..line.len-1];
    }
    return std.fmt.parseUnsigned(T, line, 10) catch unreachable;
}

const Mem = struct {
    ch: std.ArrayList(u32),
    done_ch: bool = false,
    done: bool = false,

    pub fn init() Mem {
        return .{.ch = std.ArrayList(u32).init(allocator)};
    }
};

var N: u32 = undefined;
var ms: []Mem = undefined;
var chosen: std.ArrayList(u32) = undefined;

fn dfs(m: u32) void {
    for (ms[m].ch.items) |c| {
        dfs(c);
        if (ms[c].done) {
            ms[m].done_ch = true;
        }
    }
    if (!ms[m].done_ch) {
        ms[m].done = true;
        chosen.append(m) catch unreachable;
    }
}

pub fn main() void {
    raw_in = std.io.getStdIn().reader();
    in = std.io.bufferedReader(raw_in).reader();
    raw_out = std.io.getStdOut().writer();
    out = std.io.bufferedWriter(raw_out).writer();

    N = readUInt(u32);
    ms = allocator.alloc(Mem, N) catch unreachable;
    {
        var i: usize = 0;
        while (i < N) : (i += 1) {
            ms[i] = Mem.init();
        }
    }
    {
        var i: u32 = 1;
        while (i < N) : (i += 1) {
            var c = readUInt(u32);
            ms[c-1].ch.append(i) catch unreachable;
        }
    }
    chosen = std.ArrayList(u32).init(allocator);
    dfs(0);
    std.sort.sort(u32, chosen.items, {}, comptime std.sort.asc(u32));
    out.print("{}\n", .{chosen.items.len}) catch unreachable;
    for (chosen.items) |c| {
        out.print("{} ", .{c+1}) catch unreachable;
    }
    out.writeByte('\n') catch unreachable;
    out.context.flush() catch unreachable;
}