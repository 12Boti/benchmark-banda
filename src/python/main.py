N = int(input())

class Mem:
    def __init__(self):
        self.ch = []
        self.done_ch = False
        self.done = False

ms = [Mem() for _ in range(N)]
for i in range(1, N):
    c = int(input()) - 1
    ms[c].ch.append(i)

chosen = []
def dfs(m):
    for c in ms[m].ch:
        dfs(c)
        if ms[c].done:
            ms[m].done_ch = True
    if not ms[m].done_ch:
        ms[m].done = True
        chosen.append(m)
dfs(0)
print(len(chosen))
chosen.sort()
for c in chosen:
    print(f"{c+1} ", end="")
print("")