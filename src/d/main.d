import std.stdio;
import std.algorithm;
import core.memory;

struct Mem {
    int[] ch;
    bool done_ch = false;
    bool done = false;
}

int N;
Mem[] ms;
int[] chosen;

void dfs(int m) {
    foreach (c; ms[m].ch) {
        dfs(c);
        if (ms[c].done) {
            ms[m].done_ch = true;
        }
    }
    if (!ms[m].done_ch) {
        ms[m].done = true;
        chosen ~= m;
    }
}

void main() {
    readf!" %d"(N);
    ms = new Mem[N];
    for (int i = 1; i < N; i++) {
        int c;
        readf!" %d"(c);
        c--;
        ms[c].ch ~= i;
    }
    dfs(0);
    writef!"%d\n"(chosen.length);
    sort(chosen);
    foreach (c; chosen) {
        writef!"%d "(c+1);
    }
}