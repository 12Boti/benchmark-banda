from strutils import parse_int
from algorithm import sort

type Mem = tuple
    ch: seq[int]
    done_ch: bool
    done: bool

var N = stdin.readline.parse_int

var ms: seq[Mem] = new_seq[Mem] N
for i in 1..<N:
    var c = stdin.readline.parse_int - 1
    ms[c].ch.add i

var chosen = new_seq[int]()
proc dfs(m: int) =
    for c in ms[m].ch:
        dfs c
        if ms[c].done:
            ms[m].done_ch = true
    if not ms[m].done_ch:
        ms[m].done = true
        chosen.add m
dfs 0
echo chosen.len
sort chosen
for c in chosen:
    stdout.write c+1
    stdout.write " "