#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct Node {
    bool done = false;
    bool child = false;
    vector<int> ch;
};

vector<Node> ns;
vector<int> rs;

void dfs(int n) {
    for (int c : ns[n].ch) {
        dfs(c);
        if (ns[c].done) {
            ns[n].child = true;
        }
    }
    if (!ns[n].child) {
        ns[n].done = true;
        rs.push_back(n);
    }
}

int main() {
    int N;
    cin >> N;
    ns.resize(N);

    for (int i = 1; i < N; i++) {
        int c;
        cin >> c;
        ns[c-1].ch.push_back(i);
    }

    dfs(0);
    cout << rs.size() << '\n';
    sort(begin(rs), end(rs));
    for (int r : rs) {
        cout << r+1 << ' ';
    }
    cout << '\n';
}