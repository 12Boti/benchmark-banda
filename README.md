# Building
If you are using the [nix](https://nixos.org/) package manager compatible with [nix flakes](https://nixos.wiki/wiki/Flakes):
```
nix build
```
Otherwise, you'll need to install clang, gcc, [zig](https://ziglang.org/), [nim](https://nim-lang.org/) and [ldc](https://github.com/ldc-developers/ldc), then:
```
bash bs/build.sh
```

# Running benchmarks
Using nix:
```
nix develop .#bench-shell --command bash bs/bench.sh
```
Otherwise, you'll need to install [hyperfine](https://github.com/sharkdp/hyperfine), python3 and pypy3, then:
```
bash bs/bench.sh
```

# Making plots
Using nix:
```
nix develop .#bench-shell --command bash bs/plot.sh
```
Otherwise, you'll need to install python3 and matplotlib, then:
```
bash bs/plot.sh
```
